package com.example.algashev.dto;

import java.util.Collection;

public record ProductRequest(Double price, String description, String name, String image, String category, Collection<Long> productItemIds) {

}
