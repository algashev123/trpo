package com.example.algashev.dto;

import com.example.algashev.entity.Role;

import java.util.Collection;

public record UserRequest (String name, Role role) {
}
