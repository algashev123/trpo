package com.example.algashev.dto;

import java.util.List;

public record AddProductRequest(Double price, String description, String name, String image, String category,
                                List<Long> productItemIds) {

}
