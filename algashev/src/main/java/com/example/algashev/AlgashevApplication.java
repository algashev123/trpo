package com.example.algashev;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AlgashevApplication {

	public static void main(String[] args) {
		SpringApplication.run(AlgashevApplication.class, args);
	}

}
