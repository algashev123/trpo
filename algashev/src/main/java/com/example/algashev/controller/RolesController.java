package com.example.algashev.controller;

import com.example.algashev.entity.Role;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/role")
@RequiredArgsConstructor
public class RolesController {
    @GetMapping
    public Role[] findAll() {
        return Role.values();
    }
}
