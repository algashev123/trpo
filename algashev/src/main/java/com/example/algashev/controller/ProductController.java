package com.example.algashev.controller;

import com.example.algashev.dto.AddProductRequest;
import com.example.algashev.dto.ProductRequest;
import com.example.algashev.entity.Product;
import com.example.algashev.repository.ProductRepository;
import com.example.algashev.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/products")
@RequiredArgsConstructor
public class ProductController {

    private final ProductRepository productRepository;
    private final ProductService productService;

    @GetMapping
    public List<Product> findAll() {
        return productRepository.findAll();
    }

    @GetMapping("/{id}")
    public Product findById(@PathVariable Long id) {
        return productRepository.findById(id).orElse(null);
    }


    @PostMapping("/add")
    public Product addProduct(@RequestBody AddProductRequest request) {
        return productService.addProduct(request);
    }

    @PostMapping("/{id}")
    public Product updateProduct(@PathVariable Long id, @RequestBody ProductRequest request) {
        return productService.updateProduct(id, request);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteProduct(@PathVariable Long id) {
        Product prod = this.productRepository.findById(id).orElse(null);

        productRepository.delete(prod);
    }

}
