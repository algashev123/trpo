package com.example.algashev.repository;

import com.example.algashev.entity.Product;
import com.example.algashev.entity.ProductItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Collection;
import java.util.List;

public interface ProductItemRepository extends JpaRepository<ProductItem, Long>, JpaSpecificationExecutor<ProductItem> {
    List<ProductItem> findByIdIn(Collection<Long> ids);
}