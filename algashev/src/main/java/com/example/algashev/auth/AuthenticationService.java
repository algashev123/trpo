package com.example.algashev.auth;

import com.example.algashev.dto.RegisterRequest;
import com.example.algashev.dto.AuthenticationRequest;
import com.example.algashev.entity.Client;
import com.example.algashev.entity.Role;
import com.example.algashev.repository.ClientRepository;
import com.example.algashev.security.JwtUtilities;
import jakarta.annotation.Nullable;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthenticationService {

    private final ClientRepository clientRepository;
    private final JwtUtilities jwtUtilities;
    private final PasswordEncoder passwordEncoder;
    private final AuthenticationManager authenticationManager;
    private final UserDetailsManager userDetailsManager;

    @Nullable
    public AuthenticationResponse register(RegisterRequest request) {
        Client client = clientRepository.findByEmail(request.getEmail());
        if (client != null) {
            return null;
        }

        Role role = request.getRole();

        if (role == null) {
            role = Role.USER;
        }

        client = Client.builder()
                .name(request.getName())
                .email(request.getEmail())
                .password(passwordEncoder.encode(request.getPassword()))
                .role(role)
                .build();
        var savedUser = clientRepository.save(client);
        userDetailsManager.createUser(savedUser);
        var jwtToken = jwtUtilities.generateToken(client.getUsername(), client.getRole());

        return AuthenticationResponse.builder()
                // .client(client)
                .jwtToken(jwtToken)
                .build();
    }

    public AuthenticationResponse authenticate(AuthenticationRequest request) {
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        request.getEmail(),
                        request.getPassword()
                )
        );
        var user = clientRepository.findByEmail(request.getEmail());
        var jwtToken = jwtUtilities.generateToken(user.getUsername(), user.getRole());

        return AuthenticationResponse.builder()
                //.client(user)
                .jwtToken(jwtToken)
                .build();
    }
}