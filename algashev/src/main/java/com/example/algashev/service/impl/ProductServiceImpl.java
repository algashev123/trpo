package com.example.algashev.service.impl;

import com.example.algashev.dto.AddProductRequest;
import com.example.algashev.dto.ProductRequest;
import com.example.algashev.entity.Product;
import com.example.algashev.entity.ProductItem;
import com.example.algashev.repository.ProductItemRepository;
import com.example.algashev.repository.ProductRepository;
import com.example.algashev.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;
    private final ProductItemRepository productItemRepository;

    @Override
    public Product addProduct(AddProductRequest request) {
        Product product = new Product();
        product.setName(request.name());
        product.setPrice(request.price());
        product.setCategory(request.category());
        product.setImage(request.image());
        product.setDescription(request.description());
        product = productRepository.save(product);

        if (CollectionUtils.isNotEmpty(request.productItemIds())) {
            List<ProductItem> movies = productItemRepository.findByIdIn(request.productItemIds());
            Product finalProduct = product;
            movies.forEach(movie -> movie.setProduct(finalProduct));
            productItemRepository.saveAll(movies);
        }

        return product;
    }

    @Override
    public Product updateProduct(Long productId, ProductRequest request) {
        Product product = productRepository.findById(productId).orElse(null);
        product.setName(request.name());
        product.setPrice(request.price());
        product.setCategory(request.category());
        product.setImage(request.image());
        product.setDescription(request.description());
        product = productRepository.save(product);


        if (CollectionUtils.isNotEmpty(request.productItemIds())) {
            List<ProductItem> productItems = productItemRepository.findByIdIn(request.productItemIds());

            List<ProductItem> toRemoveProduct = new ArrayList<>();

            Set<ProductItem> prevMovies = product.getProductItems();
            prevMovies.forEach(prevMovie -> {
                if (!productItems.contains(prevMovie)) {
                    prevMovie.setProduct(null);
                    toRemoveProduct.add(prevMovie);
                }
            });
            productItemRepository.saveAll(toRemoveProduct);

            Product finalProduct = product;
            productItems.forEach(movie -> movie.setProduct(finalProduct));
            productItemRepository.saveAll(productItems);
        }

        return product;
    }

}
