package com.example.algashev.service;

import com.example.algashev.dto.AddProductRequest;
import com.example.algashev.dto.ProductRequest;
import com.example.algashev.entity.Product;

public interface ProductService {
    Product addProduct(AddProductRequest request);

    Product updateProduct(Long productId, ProductRequest request);
}
