package com.example.algashev.entity;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum Role {

    USER,
    ADMIN    ;


}